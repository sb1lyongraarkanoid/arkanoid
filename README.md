Arkanoid
==

Projet de fin d'ann�e de Supinfo B.sc.1

Sujet [en]
--

Revivor is an entrainment company specialized in all-�-times video games classic revivals. They have successfully been selling small gaming devices for TV sets for 3 years and wants to take the next step. Their previous product was a
compilation of legacy video games like Pong, Pac-�-mac or even Space invaders.

Revivor now wants to launch a new product targeting middle-�-range PC�s: a compilation of al -�-times games

For this project, the Revivor executives have decided to outsource all game development to contractors. Each contractor will undertake the development of one video game of the collection.

Revivor not only wants the game collection to be a simple rewrite of existing games. They also want to add new features. The contract stipulates that each contractor must provide at least two innovative features to the game they rewrite. A network-�-based multiplayer mode would be greatly appreciated.

Your team has been chosen to rewrite the all-�-times classic �Arkanoid�, the well-�-known brick breaker game. You�ll also have to create the related website.

You have just two technical constraints: the video game must be developed with C language and running inside the website, using the brand new NaCl technology (Google Native Client).

Equipe
--

L'�quipe est compos� de 3 personnes :
 - GARDET Julien (Jitrixis)
 - RIVET Raphael
 - ALMANINI Maxence

Tous 3 en premi�re ann�es de licenses � Supinfo Lyon

Licenses
--

Propri�taire

    