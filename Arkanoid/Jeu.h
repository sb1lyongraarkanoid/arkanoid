#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED

void initData(int nW, int nH, int bW, int bH, int wW, int wH);

void initValue();

int getBallX(int nb);

int getBallY(int nb);

int getBallGo(int nb);

void dropBall(int nb);

int getBallVel();

int goBall(int nb);

void startBall();

int getPadX();

int getPadSize();

int getPadVel();

void goPad(int dir);

void goPadJoy(int vel);

int pointIsPositive(int k, int l, int nb);

int sideIsPositive(int k, int l, int nb);

int getIsDeed();

int getLife();

void levelUp();

int getLevel();

void resetLevel();

int getHasLevelUp();

void testColisionsBall(int nb);

void addRandomBonus(int x, int y);

int getBonusNb();

int getBonusN(int n);

int getBonusX(int n);

int getBonusY(int n);

void goBonus();

int getDeleteBonusNb();

int getDeleteBonusX();

int getDeleteBonusY();

void goDeleteBonus();

int getPadBonusFlag();

int getIsExplosive();

int getTimeVelBall();

int getTimeSizePad();

int getHasSticky();

int getHasDoubleBall();

int getScore();

#endif //JEU_H_INCLUDED