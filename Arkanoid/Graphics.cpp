#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>
#include <time.h>
#include <SDL_ttf.h>
#include <Windows.h>
#include "Graphics.h"
#include "Donn�e.h"
#include "Jeu.h"

SDL_Surface *ecran, *save, *saveBonus, *background;
SDL_Rect positionSave[4];

int maxLevel = 15;

int numberWidth, numberHeight, brickWidth, brickHeight, windowsWidth, windowsHeight;

int marginX = -28, marginY = 0, windAdd = 95, statSize = 150;

//Chamegement d'�tat
int hasSave = 0;
int needBG = 1;
int needBrick = 1;
int needBrickE = 0;
int needErase = 0;
int needPad = 1;

//BONUS STATE
//effaceer pad surpl�mentaire lors d'un bonus
int erasePadBonus = 0;

//OLD DATA
int **oldDonnee;
int **firstDonnee;

//PLUS
SDL_Joystick *joystick;

int IDuser = 170535;

int joystickConnected = 0;

//Initialisation de la fen�tre.

void initWindows(int sW, int sH, int bW, int bH){
	numberWidth = sW;
	numberHeight = sH;
	brickWidth = bW;
	brickHeight = bH;

	windowsWidth = numberWidth * brickWidth + 10;
	windowsHeight = numberHeight * brickHeight + 10 ;

	// D�marrage de la SDL
	
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);
	TTF_Init();

	//Image de la fen�tre
	SDL_WM_SetIcon(IMG_Load("favicone.png"), NULL);

	ecran = SDL_SetVideoMode(windowsWidth + windAdd, windowsHeight, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	save = SDL_CreateRGBSurface(SDL_HWSURFACE, windowsWidth, windowsHeight, 32, 0, 0, 0, 0);
	saveBonus = SDL_CreateRGBSurface(SDL_HWSURFACE, windowsWidth, windowsHeight, 32, 0, 0, 0, 0);

    if (ecran == NULL)
    {
        fprintf(stderr, "Impossible de charger le mode vid�o : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
	
	SDL_WM_SetCaption("!Breakout!", NULL);

	//JOYSTICK
	if(SDL_NumJoysticks() != 0){
		joystick = SDL_JoystickOpen(0);
		SDL_JoystickEventState(SDL_ENABLE);
		joystickConnected = 1;
	}

    //pause();
	waitScreen("ImageMenu.png", 1);
	demanderID();

	/*
	 * 
	 */
	//SDL_BlitSurface(IMG_Load("fondcb.png"), NULL, ecran, NULL);
	setBackground(1);

	
	//Uint32 bleuVert = SDL_MapRGB(ecran->format, 17, 206, 112);
	//Uint32 white = SDL_MapRGB(ecran->format, 255, 255, 255);

	//OLD DATA INIT
	oldDonnee = (int**) malloc(numberHeight*sizeof(int*));
	int i, j;
    for (i=0;i<numberHeight;i++){
		oldDonnee[i] = (int*) malloc(numberWidth*sizeof(int));
		for(j=0;j<numberWidth;j++){
            oldDonnee[i][j] = 0;
        }
    }

	firstDonnee = (int**) malloc(numberHeight*sizeof(int*));
    for (i=0;i<numberHeight;i++){
		firstDonnee[i] = (int*) malloc(numberWidth*sizeof(int));
		for(j=0;j<numberWidth;j++){
            firstDonnee[i][j] = 0;
        }
    }
}

void addBrick(int x, int y){
	
	//DEBUG
	//printf("\n%d/%d = %d (%dx%d)", x, y, getDonnee(x, y), 5 + x * brickWidth, 5 + y * brickHeight);
	int brickState = getDonnee(x, y);
	if(brickState != 0 && brickState != 9){
		//Init stuff
		SDL_Surface *brick = NULL;
		SDL_Rect position;

		//Setting stuff

		//brick = SDL_CreateRGBSurface(SDL_HWSURFACE, brickWidth, brickHeight, 32, 0, 0, 0, 0);
		//SDL_FillRect(brick, NULL, white);
		if(brickState == 5){
			brick = IMG_Load("wall.png");
		}else if(brickState == 6){
			brick = IMG_Load("wallhaut.png");
		}else if(brickState == 7){
			brick = IMG_Load("walldroit.png");
		}else if(brickState == 4){
			brick = IMG_Load("briqueindestructible.png");
		}else if(brickState == 3){
			brick = IMG_Load("briquetroiscoupnormal.png");
		}else if(brickState == 2){
			brick = IMG_Load("Briquedeuxcoupnormal.png");
		}else if(brickState == 1){
			int ran = (x + y)%4;
			if(ran == 0){
				brick = IMG_Load("briqueuncoup0.png");
			}else if (ran == 1){
				brick = IMG_Load("briqueuncoup1.png");
			}else if (ran == 2){
				brick = IMG_Load("briqueuncoup2.png");
			}else if (ran == 3){
				brick = IMG_Load("briqueuncoup3.png");
			}else{
				brick = IMG_Load("Briquetest.png");
			}
		}else{
			brick = IMG_Load("Briquetest.png");	
		}

		position.x = marginX + 5 + y * brickWidth;
		position.y = marginY + 5 + x * brickHeight;

		/**
		 * DEBUG
		 */
		/*SDL_Surface *surface = NULL;
		surface = SDL_CreateRGBSurface(SDL_HWSURFACE, brickWidth, brickHeight, 32, 0, 0, 0, 0);
		SDL_FillRect(surface, NULL, SDL_MapRGB(ecran->format, 17, 206, 112));
		merge2screen(surface, position);*/
		
		//SDL_BlitSurface(brick, NULL, ecran, &position);
		merge2screen(brick, position);
		merge2saveBonus(brick, position);

		//Sending stuff and free

		//send2Screen();

		SDL_FreeSurface(brick);
	}
}

void displayAllBrick(){
	int i, j;
	for(i = 0; i < numberHeight; i++){
		for(j = 0; j < numberWidth; j++){
			addBrick(i, j);
		}
	}
	SDL_BlitSurface(ecran, NULL, save, NULL);
	SDL_BlitSurface(ecran, NULL, saveBonus, NULL);
}

void setBackground(int ID){
	needBG = 1;
	if(ID == 1){
		SDL_BlitSurface(IMG_Load("fondcb.png"), NULL, ecran, NULL);
		background = IMG_Load("fondcb.png");
		saveBonus = IMG_Load("fondcb.png");
	}else{
		SDL_BlitSurface(IMG_Load("fondcb.png"), NULL, ecran, NULL);
		background = IMG_Load("fondcb.png");
		saveBonus = IMG_Load("fondcb.png");
	}
}

void displayBall(){
	int k;
	if(hasSave == 0){
		SDL_BlitSurface(ecran, NULL, save, NULL);
	}
	hasSave = 1;
	for(k = 0; k < 4; k++){
		if(getBallGo(k)){
			if(goBall(k) == 1){
			needBrickE = 1;
			}
			SDL_Rect position;
			position.x = marginX + getBallX(k);
			position.y = marginY + getBallY(k);
			position.w = 22;
			position.h = 22;
			if(hasSave != 0){
				SDL_BlitSurface(save, &positionSave[k], ecran, &positionSave[k]);
			}
			positionSave[k] = position;
			merge2screen(IMG_Load("balle.png"), position);
		}else{
			if(getBallX(k) != 0 || getBallY(k) != 0){
				SDL_Rect position;
				position.x = marginX + getBallX(0);
				position.y = marginY + getBallY(0);
				position.w = 22;
				position.h = 22;
				if(hasSave != 0){
					SDL_BlitSurface(save, &positionSave[k], ecran, &positionSave[k]);
				}
				positionSave[k] = position;
				SDL_BlitSurface(background, &position, ecran, &position);
				dropBall(k);
			}
		}
	}
}

void displayPad(){
	SDL_Surface *surface;
	SDL_Rect position;
	//Clear
	position.x = marginX + getPadX() - getPadVel();
	position.y = marginY + 5 + (numberHeight - 1) * brickHeight + 1;
	position.w = getPadSize() + getPadVel()*2;
	position.h = 18;
	if(erasePadBonus == 1){
		erasePadBonus = 0;
		position.w = 210+getPadVel()*2;
		position.x -= (210 - getPadSize())/2-getPadVel();
	}
	SDL_BlitSurface(background, &position, ecran, &position);
	SDL_BlitSurface(background, &position, save, &position);
	SDL_BlitSurface(background, &position, saveBonus, &position);
	//Set
	position.x = marginX + getPadX();
	position.y = marginY + 5 + (numberHeight - 1) * brickHeight + 1;
	if(getPadSize() == 70){
		surface = IMG_Load("padrouge70.png");
	}else if(getPadSize() == 140){
		surface = IMG_Load("padrouge140.png");
	}else if(getPadSize() == 210){
		surface = IMG_Load("padrouge210.png");
	}else{
		surface = IMG_Load("padtest.png");
	}
	merge2screen(surface, position);
	merge2save(surface, position);
	merge2saveBonus(surface, position);
}

void oldDataExchange(){
	int i, j;
	for(i = 0; i< numberHeight; i++){
		for(j = 0; j < numberWidth; j++){
			oldDonnee[i][j] = getDonnee(i, j);
		}
	}
}

void firstDataExchange(){
	int i, j;
	for(i = 0; i< numberHeight; i++){
		for(j = 0; j < numberWidth; j++){
			firstDonnee[i][j] = getDonnee(i, j);
			oldDonnee[i][j] = getDonnee(i, j);
		}
	}
}

void brickExchange(){
	int i, j;
	int change = 0;
	for(i = 0; i< numberHeight; i++){
		for(j = 0; j < numberWidth; j++){
			if(oldDonnee[i][j] != getDonnee(i, j)){
				//Init stuff
				SDL_Surface *brick = NULL;
				SDL_Rect position;

				int brickState = getDonnee(i, j);

				//SI state 1 � 3
				if(brickState >= 1 && brickState <= 2){
					if(brickState == 2 && firstDonnee[i][j] == 3){
						brick = IMG_Load("briquetroiscoupamoche1.png");
					}else if(brickState == 1 && firstDonnee[i][j] == 3){
						brick = IMG_Load("briquetroiscoupamoche2.png");
					}else if(brickState == 1 && firstDonnee[i][j] == 2){
						brick = IMG_Load("briquedeuxcoupamoche.png");
					}

					position.x = marginX + 5 + j * brickWidth;
					position.y = marginY + 5 + i * brickHeight;
					merge2screen(brick, position);
					merge2save(brick, position);
					merge2saveBonus(brick, position);
				}

				//SI state 0
				if(brickState == 0){
					needErase = 1;
				}

				SDL_FreeSurface(brick);
				change = 1;
			}
		}
	}
	if(change == 1 && needErase == 0){
		oldDataExchange();
	}
}

void eraseBrick(){
	int i, j;
	for(i = 0; i< numberHeight; i++){
		for(j = 0; j < numberWidth; j++){
			if(oldDonnee[i][j] != getDonnee(i, j) && getDonnee(i, j) == 0){
				SDL_Rect position;
				position.x = marginX + 5 + j * brickWidth;
				position.y = marginY + 5 + i * brickHeight;
				position.w = brickWidth;
				position.h = brickHeight;
				SDL_BlitSurface(background, &position, ecran, &position);
				SDL_BlitSurface(background, &position, save, &position);
				SDL_BlitSurface(background, &position, saveBonus, &position);
			}
		}
	}
	oldDataExchange();
}

void diplayBonus(){
	SDL_Rect position;
	int i;
	for(i = 0; i < getBonusNb(); i++){
		position.x = marginX + getBonusX(i);
		position.y = marginY + getBonusY(i)-1;
		position.h = brickHeight;
		position.w = brickWidth;
		SDL_BlitSurface(saveBonus, &position, ecran, &position);
		SDL_BlitSurface(saveBonus, &position, save, &position);
		position.y++;
		switch (getBonusN(i))
		{
		case 1:	
			merge2screen(IMG_Load("Bonus_BigPaddle.png"), position);
			merge2save(IMG_Load("Bonus_BigPaddle.png"), position);
			break;
		case 2:	
			merge2screen(IMG_Load("Bonus_MiniPaddle.png"), position);
			merge2save(IMG_Load("Bonus_MiniPaddle.png"), position);
			break;
		case 3:	
			merge2screen(IMG_Load("Bonus_double.png"), position);
			merge2save(IMG_Load("Bonus_double.png"), position);
			break;
		case 4:	
			merge2screen(IMG_Load("Bonus_Accel.png"), position);
			merge2save(IMG_Load("Bonus_Accel.png"), position);
			break;
		case 5:	
			merge2screen(IMG_Load("Bonus_Ral.png"), position);
			merge2save(IMG_Load("Bonus_Ral.png"), position);
			break;
		case 6:	
			merge2screen(IMG_Load("Bonus_sticky.png"), position);
			merge2save(IMG_Load("Bonus_sticky.png"), position);
			break;
		case 7:	
			merge2screen(IMG_Load("Bonus_Dynamite.png"), position);
			merge2save(IMG_Load("Bonus_Dynamite.png"), position);
			break;
		case 8:
			merge2screen(IMG_Load("Bonus_Mort.png"), position);
			merge2save(IMG_Load("Bonus_Mort.png"), position);
			break;
		case 9:
			merge2screen(IMG_Load("Bonus_Vie.png"), position);
			merge2save(IMG_Load("Bonus_Vie.png"), position);
			break;
		default:
			break;
		}


	}
	for(i = 0; i < getDeleteBonusNb(); i++){
		position.x = marginX + getDeleteBonusX();
		position.y = marginY + getDeleteBonusY() - 1;
		position.h = brickHeight + 1;
		position.w = brickWidth;
		SDL_BlitSurface(saveBonus, &position, ecran, &position);
		SDL_BlitSurface(saveBonus, &position, save, &position); 
		goDeleteBonus();
	}
	goBonus();
}

void displayStat(){
	TTF_Font *police = NULL;
	TTF_Font *minipolice = NULL;
	police = TTF_OpenFont("Oswald-Regular.ttf", 35);
	minipolice = TTF_OpenFont("Oswald-Regular.ttf", 15);
	SDL_Color black = {0, 0, 0};
	SDL_Color red = {255, 0, 0};
	SDL_Surface *bg;
	SDL_Surface *bar;
	SDL_Rect position;
	position.x = windowsWidth + windAdd - statSize;
	position.y = 0;
	bg = SDL_CreateRGBSurface(SDL_HWSURFACE, statSize, windowsHeight, 32, 0, 0, 0, 0);
	SDL_FillRect(bg, NULL, SDL_MapRGB(ecran->format, 200, 200, 255));
	merge2screen(bg, position);
	SDL_FreeSurface(bg);
	
	//Texte
	SDL_Surface *texte;
	texte = TTF_RenderText_Blended(police, "!Breakout!", red);
	position.x += 5;
	merge2screen(texte, position);
	position.x -= 5;
	//USer ID
	char id[30];
	sprintf(id, "User : %d", IDuser);
	texte = TTF_RenderText_Blended(minipolice, id, black);
	position.y += 60;
	position.x += 5;
	merge2screen(texte, position);
	position.x -= 5;
	//Score
	char score[30];
	sprintf(score, "Score : %d", getScore());
	texte = TTF_RenderText_Blended(minipolice, score, black);
	position.y += 25;
	position.x += 5;
	merge2screen(texte, position);
	position.x -= 5;
	//Level
	char level[30];
	sprintf(level, "Level : %d", getLevel());
	texte = TTF_RenderText_Blended(minipolice, level, black);
	position.y += 25;
	position.x += 5;
	merge2screen(texte, position);
	position.x -= 5;

	//nombre de vie
	int i;
	position.y += 30;
	position.x -= 10;
	for(i = 0; i < getLife(); i++){
		position.x += 20;
		merge2screen(IMG_Load("balle.png"), position);
	}
	position.x = windowsWidth + windAdd - 90;
	position.y -= 5;
	if(getIsExplosive() != 0){
		merge2screen(IMG_Load("Bonus_Dynamite.png"), position);
	}
	position.y += 3;
	position.x += 30;
	if(getHasDoubleBall() != 1){
		merge2screen(IMG_Load("Bonus_double.png"), position);
	}
	position.y += 10;

	//Count sticky
	if(getHasSticky() != 0){
		bar = SDL_CreateRGBSurface(SDL_HWSURFACE, (getHasSticky() <= 0)?0:getHasSticky()*(statSize - 40)/15, 10, 32, 0, 0, 0, 0);
		position.x = windowsWidth + windAdd - statSize;
		position.y += 30;
		if(getHasSticky() > 0){
			merge2screen(IMG_Load("Bonus_sticky.png"), position);
		}
		position.x = windowsWidth + windAdd - statSize + 20;
		position.y += 25;
		SDL_FillRect(bar, NULL, SDL_MapRGB(ecran->format, 100, 200, 100));
		merge2screen(bar, position);
		SDL_FreeSurface(bar);
	}

	//bar taille pad
	if(getPadSize() != 140){
		bar = SDL_CreateRGBSurface(SDL_HWSURFACE, (getTimeSizePad() <= 0)?0:getTimeSizePad()*(statSize - 40)/60, 10, 32, 0, 0, 0, 0);
		position.x = windowsWidth + windAdd - statSize + 10;
		position.y += 30;
		if(getPadSize() > 140){
			merge2screen(IMG_Load("Bonus_BigPaddle.png"), position);
		}else if(getPadSize() < 140){
			merge2screen(IMG_Load("Bonus_MiniPaddle.png"), position);
		}
		position.x += 10;
		position.y += 25;
		SDL_FillRect(bar, NULL, SDL_MapRGB(ecran->format, 200, 100, 100));
		merge2screen(bar, position);
		SDL_FreeSurface(bar);
	}

	//bar vitesse balleSDL_Surface *bar;
	if(getBallVel() != 2){
		bar = SDL_CreateRGBSurface(SDL_HWSURFACE, (getTimeVelBall() <= 0)?0:getTimeVelBall()*(statSize - 40)/60, 10, 32, 0, 0, 0, 0);
		position.x = windowsWidth + windAdd - statSize;
		position.y += 30;
		if(getBallVel() == 1){
			merge2screen(IMG_Load("Bonus_Ral.png"), position);
		}else if(getBallVel() > 2){
			int i;
			for(i = 0; i < getBallVel() - 2; i++){
				merge2screen(IMG_Load("Bonus_Accel.png"), position);
				position.x += 20;
			}
		}
		position.x = windowsWidth + windAdd - statSize + 20;
		position.y += 25;
		SDL_FillRect(bar, NULL, SDL_MapRGB(ecran->format, 200, 100, 200));
		merge2screen(bar, position);
		SDL_FreeSurface(bar);
	}

	//help
	if(!joystickConnected){
		position.y = 375;
		position.x = windowsWidth + windAdd - statSize + 55;
		texte = TTF_RenderText_Blended(minipolice, "Aide : ", black);
		merge2screen(texte, position);
		position.x -= 50;
		position.y += 20;
		texte = TTF_RenderText_Blended(minipolice, "Up : lancer la balle ", black);
		merge2screen(texte, position);
		position.y += 20;
		texte = TTF_RenderText_Blended(minipolice, "Fl�ches : d�placer le pad ", black);
		merge2screen(texte, position);
		position.y += 20;
		texte = TTF_RenderText_Blended(minipolice, "Echap : Quiter le jeu ", black);
		merge2screen(texte, position);
	}else{
		position.y = 375;
		position.x = windowsWidth + windAdd - statSize + 55;
		texte = TTF_RenderText_Blended(minipolice, "Aide : ", black);
		merge2screen(texte, position);
		position.x -= 50;
		position.y += 20;
		texte = TTF_RenderText_Blended(minipolice, "A : lancer la balle ", black);
		merge2screen(texte, position);
		position.y += 20;
		texte = TTF_RenderText_Blended(minipolice, "LT / RT : d�placer le pad ", black);
		merge2screen(texte, position);
		position.y += 20;
		texte = TTF_RenderText_Blended(minipolice, "B : Quiter le jeu ", black);
		merge2screen(texte, position);
	}

	//Controler display
	if(joystickConnected){
		position.x = windowsWidth + windAdd - 60;
		position.y = windowsHeight - 45;
		merge2screen(IMG_Load("logo_manette.png"), position);
	}else{
		position.x = windowsWidth + windAdd - 60;
		position.y = windowsHeight - 45;
		merge2screen(IMG_Load("logo_keyboard.png"), position);
	}

	TTF_CloseFont(police);
	TTF_CloseFont(minipolice);
	SDL_FreeSurface(texte);
}

void clearScreen(){
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
}

void merge2screen(SDL_Surface* surface, SDL_Rect position){
	SDL_BlitSurface(surface, NULL, ecran, &position);
}

void merge2save(SDL_Surface* surface, SDL_Rect position){
	SDL_BlitSurface(surface, NULL, save, &position);
}

void merge2saveBonus(SDL_Surface* surface, SDL_Rect position){
	SDL_BlitSurface(surface, NULL, saveBonus, &position);
}

void send2Screen(){
	SDL_Flip(ecran);
}

void arkanoidFullDisplay(){
	//Background
	if(needBG == 1){
		setBackground(1);
		needBG = 0;
	}
	//Bricks
	if(needBrick == 1){
		displayAllBrick();
		needBrick = 0;
	}
	//Pad
	if(needPad == 1){
		displayPad();
		needPad = 0;
	}
	//BrickExchange
	if(needBrickE == 1){
		brickExchange();
		needBrickE = 0;
	}
	//Bonus
	diplayBonus();
	//Ball
	displayBall();
	//EraseBrick
	if(needErase == 1){
		eraseBrick();
		needErase = 0;
	}
	//Statistic
	displayStat();
	//Envoie � l'�cran
	send2Screen();
}

void closeWindows(){
    SDL_FreeSurface(ecran);
	SDL_FreeSurface(save);
	SDL_FreeSurface(background);
	SDL_JoystickClose(joystick);
	SDL_Quit();
	TTF_Quit();
}

/**
 * Met en pause le syst�me jusqu'� ce qu'un �v�nement apparaisse dans la SDL
 */

void event()
{
    int continuer = 1;
    SDL_Event event;
	initData(numberWidth, numberHeight, brickWidth, brickHeight, windowsWidth, windowsHeight);
	//END DEUBUG
	firstDataExchange();
	SDL_EnableKeyRepeat(10, 10);
 
		//DEBUG SPEEDLESS
		int speed = 15;
		int fastspeed = 10;
		int stop = 0;
    while (continuer)
    {
		//IS DEED :
		if(getIsDeed()){
			waitScreen("ImageGameOver.png", 0);
			initData(numberWidth, numberHeight, brickWidth, brickHeight, windowsWidth, windowsHeight);
			needBG = 1;
			needBrick = 1;
			needPad = 1;
			firstDataExchange();
		}
		//IS LEVELUP
		if(getNumberOfbrick(numberWidth, numberHeight) <= 0){
			levelUp();
		}

		//NEED REDRAW PAD ON BONUS
		if(getPadBonusFlag()){
			needPad = 1;
			erasePadBonus = 1;
		}

		//END LEVEL
		if(getLevel() > maxLevel){
			waitScreen("ImageFinJeuReussi.png", 1);
			closeWindows();
		}

		//LEVEL UP
		if(getHasLevelUp()){
			waitScreen("ImageFinNiveau.png", 0);
			needBG = 1;
			needBrick = 1;
			needPad = 1;
			firstDataExchange();
		}

		//DEBUG SPEED
		int curspeed = speed;

		//EVENT
        SDL_PollEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
					case SDLK_ESCAPE:
						continuer = 0;
						break;
					case SDLK_LEFT:
						goPad(0);
						needPad = 1;
						break;
					case SDLK_RIGHT:
						goPad(1);
						needPad = 1;
						break;
					case SDLK_UP:
					case SDLK_SPACE:
						startBall();
						break;
					case SDLK_RSHIFT:
						curspeed = fastspeed;
						break;
					case SDLK_F2:
						speed = 1;
						break;
					case SDLK_F3:
						speed = 10;
						break;
					case SDLK_F4:
						speed = 30;
						break;
					case SDLK_F5:
						speed = 100;
						break;
					case SDLK_F6:
						speed = 1000;
						break;
					case SDLK_f:
						curspeed = 1;
						break;
					case SDLK_g:
						fastspeed = (fastspeed == 10)?500:10;
						break;
					case SDLK_s:
						stop = !stop;
						break;
					case SDLK_F1:
						initData(numberWidth, numberHeight, brickWidth, brickHeight, windowsWidth, windowsHeight);
						firstDataExchange();
						needBG = 1;
						needBrick = 1;
						needPad = 1;
						break;
				}
				break;
			case SDL_JOYAXISMOTION:
				if(event.jaxis.axis == 2){
					int vel = -event.jaxis.value/3750;
					if(vel > 8){
						vel = 8;
					}
					if(vel < -8){
						vel = -8;
					}
					goPadJoy(vel);
					needPad = 1;
				}
				break;
			case SDL_JOYBUTTONDOWN:
				switch (event.jbutton.button)
				{
				case 0:
					startBall();
					break;
				case 1:
					continuer = 0;
					break;
				case 7:
					stop = !stop;
					break;
				}
				break;
        }

		//PLAY GAME
		if(!stop)
			arkanoidFullDisplay();
		

		//Delay
		SDL_Delay(curspeed);
    }

}

void waitScreen(char file[50], int hideStat){
	SDL_Event event;
	SDL_Rect position;
	position.x = -statSize/2;
	position.y = 0;
	if(hideStat){
		position.x = 0;
	}
	SDL_BlitSurface(IMG_Load(file), NULL, ecran, &position);
	if(!hideStat){
		displayStat();
	}
	SDL_Flip(ecran);
	while(true){
		SDL_WaitEvent(&event);
		if((event.type == SDL_KEYDOWN && (event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_SPACE || event.key.keysym.sym == SDLK_UP)) || (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) || (event.type == SDL_JOYBUTTONDOWN && (event.jbutton.button == 0 || event.jbutton.button == 7))){
			return;
		}
		if((event.type == SDL_QUIT) || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) || (event.type == SDL_JOYBUTTONDOWN && event.jbutton.button == 1)){
			closeWindows();
		}
	}
}

void demanderID(){
	SDL_Event event;
	SDL_Rect position;
	SDL_Rect positionText;
	TTF_Font *police = NULL;
	police = TTF_OpenFont("Oswald-Regular.ttf", 75);
	SDL_Color black = {0, 0, 0};//Texte
	SDL_Surface *texte;
	positionText.x = 295;
	positionText.y = 185;
	position.x = 0;
	position.y = 0;
	SDL_BlitSurface(IMG_Load("ImageIDbooster.png"), NULL, ecran, &position);
	SDL_Flip(ecran);
	int continuer = 1;
	int booster = 0;
	IDuser = booster;
	while(continuer){
		SDL_WaitEvent(&event);
		switch(event.type)
        {
            case SDL_QUIT:
				closeWindows();
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
					case SDLK_ESCAPE:
						closeWindows();
						break;
					case SDLK_UP:
					case SDLK_SPACE:
					case SDLK_RETURN:
						if(booster != 0){
							continuer = 0;
						}
						break;
					case SDLK_0:
						booster *= 10;
						break;
					case SDLK_1:
						booster *= 10;
						booster += 1;
						break;
					case SDLK_2:
						booster *= 10;
						booster += 2;
						break;
					case SDLK_3:
						booster *= 10;
						booster += 3;
						break;
					case SDLK_4:
						booster *= 10;
						booster += 4;
						break;
					case SDLK_5:
						booster *= 10;
						booster += 5;
						break;
					case SDLK_6:
						booster *= 10;
						booster += 6;
						break;
					case SDLK_7:
						booster *= 10;
						booster += 7;
						break;
					case SDLK_8:
						booster *= 10;
						booster += 8;
						break;
					case SDLK_9:
						booster *= 10;
						booster += 9;
						break;
					case SDLK_BACKSPACE:
						booster /= 10;
						break;
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					if(booster != 0){
						continuer = 0;
					}
					break;
				}
				break;
			case SDL_JOYBUTTONDOWN:
				switch (event.jbutton.button)
				{
				case 0:
				case 7:
					if(booster != 0){
						continuer = 0;
					}
					break;
				case 1:
					closeWindows();
					break;
				}
				break;
		}

		if(booster > 999999){
			booster /= 10;
			IDuser = booster;
		}

		if(booster != IDuser){
			IDuser = booster;
			SDL_BlitSurface(IMG_Load("ImageIDbooster.png"), NULL, ecran, &position);
			if(booster != 0){
			char string[10];
				sprintf(string, "%d", booster);
				texte = TTF_RenderText_Blended(police, string, black);
				SDL_BlitSurface(texte, NULL, ecran, &positionText);
			}
			SDL_Flip(ecran);
		}
	}
	

	TTF_CloseFont(police);
	SDL_FreeSurface(texte);
}