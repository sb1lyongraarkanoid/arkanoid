#include <stdio.h>
#include <Windows.h>
#include <SDL.h>
#include <SDL_image.h>
#include "Donn�e.h"
#include "Graphics.h"

/**********************************************************************************************************************************
***********************************************************************************************************************************
**																																 **
**																																 **
** 								   .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..											 **
** 							..';cldkO00KXNNNNXXXKK000OOkkkkkxxxxxddoooddddddxxxxkkkkOO0XXKx:.									 **
** 							  .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'							 **
** 						 .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.						 **
** 					  .ckKKkc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.						 **
** 				   .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.					 **
** 				  :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.					 **
** 				 .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo					 **
** 				 .0K.         .;.              ;'              ';                      .'...'.           .oXX:					 **
** 				.oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:					 **
** 			   .dNX:               ......       ;.                'cxOKK0OXWWWWWWWNX0kc.                    :KXd.				 **
** 			 .l0N0;             ;d0KKKKKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.				 **
** 		   .lKNKl...'......'. .dXWN0kkk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNKkxONK: .,:c:.      .';;;;:lk0XXx;			 **
** 		  :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:okKX0kdl:::;'',;coxkkd, ...'. ...'''.......',:lxKO:.		 **
** 		 oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,		 **
** 		'KW0,:,.,:..,oxkkkdl;'.                'KK'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.		 **
** 		;XNd,;  ;. l00kxoooxKXKx:..ld:         ;KK'                             .:dkO000000Okxl;.   c0;      :KK;   .  ;XXc		 **
** 		'XXdc.  :. ..    '' 'kNNNKKKk,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.	 **
** 		.kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;okKXK0KNXx;.   .0X:  ,.  lNX'	 **
** 		 ,KKdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXKk:.'KX;  '   dNX.	 **
** 		  :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxkkkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.	 **
** 		   cXXd,,;:, .oXWNNKo'    .'..  .'.'dKk;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.		 **
** 			cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOKKK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.		 **
** 			 lXNo    cXWWWXooNWNXKko;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,		 **
** 			 .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00KKKK0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,			 **
** 			  'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;			 **
** 			  .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNKKNNWWWWNKkl;kNk. .cKXo.           .ON0;				 **
** 			  .xNd   cNWWWWWWWWKOkKNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.				 **
** 			  .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.				 **
** 			  .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :KK,					 **
** 			  .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:					 **
** 			  .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,					 **
** 			  :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.					 **
** 			 .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,						 **
** 			 .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.							 **
** 			 ,XX:             ..';cldxkOO0KKKXXXXXXXXXXKKKKK00Okxdol:;'..   .';::,..':llc,..'lkKXkc.							 **
** 			 :NX'    .     ''            ..................             .,;:;,',;ccc;'..'lkKX0d;.								 **
** 			 lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,lkKX0d:.									 **
** 			.oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.										 **
** 			.dN0.      .;;,..       ....                ..''''''''....     .:dOKKko;.											 **
** 			 lNK'         ..,;::;;,'.........................           .;d0X0kc'.												 **
** 			 .xXO'                                                 .;oOK0x:.													 **
** 			  .cKKo.                                    .,:oxkkkxk0K0xc'.														 **
** 				.oKKkc,.                         .';cok0XNNNX0Oxoc,.															 **
** 				  .;d0XX0kdlc:;,,,',,,;;:clodkO0KK0Okdl:,'..																	 **
** 					  .,coxO0KXXXXXXXKK0OOxdoc:,..																				 **
** 								...																								 **
**																																 **
**																																 **
***********************************************************************************************************************************
**********************************************************************************************************************************/

int main(int argc, char *argv[]) {
	
	FreeConsole();

	//final windows size (1030x650 -> usable 1025x645) final brick size (68x26 -> draw 65x23)
	int brickNumberWidth = 15; //15
	int brickNumberHeight = 25; //25
	int brickSizeWidth = 68; //65
	int brickSizeHeight = 21; //25

	initWindows(brickNumberWidth, brickNumberHeight, brickSizeWidth, brickSizeHeight);
	//initialiser(brickNumberWidth, brickNumberHeight);
	
	event();
	closeWindows();
	return EXIT_SUCCESS;

	//DEBUG
	/*
    int choix;
    int hauteur, largeur;

    printf("\t\t\t\t  Casse-Brique\n\n");
    printf("\t\t\t\tPour jouer taper 1\n\n");
    printf("\t\t\t\t  Sinon taper 2\n");
    scanf("%d",&choix);
    system("cls");

    if (choix==1){
        system("cls");

        do{
        printf("\n  Veuillez choisir la hauteur de la fenetre du casse brique entre 15 et 35:");
        scanf("%d", &hauteur);
        }while(hauteur <15 || hauteur >35 );

        do{
        printf("\n  Veuillez choisir la largeur de la fenetre du casse brique entre 13 et 32:");
        scanf("%d", &largeur);
        }while(largeur <13 || largeur >32 );

        initialiser(largeur, hauteur);
        affichage(largeur, hauteur);
        system("pause");
    }else{
        exit(0);
    }
	*/
	//Ending DEBUG
}