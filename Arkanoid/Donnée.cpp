#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Donn�e.h"
int **Donnee;

int initialiser(int largeur,int hauteur, int level) // va stocker les donn�es du tableau
{
    //Initialise les variables
    int i,j, compteurBrique =0;
    srand(time(NULL));

    Donnee = (int**) malloc(hauteur*sizeof(int*));

    //Initialise le tableau entier � 0
    for (i=0;i<hauteur;i++){
        Donnee[i] = (int*) malloc(largeur*sizeof(int));
        for(j=0;j<largeur;j++){
            Donnee[i][j] = 0;
        }
    }

    //OU Initialise les bords
    for (i=0;i<hauteur;i++){
        for(j=0;j<largeur;j++){
            if (i<hauteur-1 && j==0){//valeur des bords de gauche 
                Donnee[i][j] = Donnee[i][j]+5;
            }else if(i==0){ // valeur des bords sup'
                Donnee[i][j] = Donnee[i][j]+6;
            }else if(j==largeur-1 && i<hauteur-1){ // valeur des bords de droite
                Donnee[i][j] = Donnee[i][j]+7;
            }else{
                Donnee[i][j] = 0;
            }
            Donnee[0][largeur-1]=7;//definit la valeur pour le coin sup droit
           Donnee[0][0]=5;//definit la valeur pour le coin sup gauche
        }
    }

	//Initialis� un fichier
	
	if(level >= 0){
		//charger level
		if(initialiserFichier(level)){
			return 0;
		}
	}

    //Initialise al�atoirement les briques
    for (i=2;i<hauteur-10;i++){ // attribution des briques
        for(j=3;j<largeur-3;j++){
            Donnee[i][j]=rand()%5;

            if (compteurBrique >=hauteur*3){ // limite le nombre de brique
                Donnee[i][j]=0;
            }else if (compteurBrique <hauteur*3 && Donnee[i][j]>=1 || Donnee[i][j]<=4){
                /* pour que la case garde la valeur d'une brique (4/7)
                il faut que le compteur soit inf�rieur au maximum de
                brique demand� sinon il prend la valeur du vide (0)*/
                compteurBrique++;
            }else {
                Donnee[i][j]=0;
            }
        }
    }

    //Initialise le palet
    for(j=0; j<largeur; j++){
        Donnee[hauteur-1][j] = 9;
    }
    /*for(j=(largeur/2)-2;j<(largeur/2)+2;j++){
        Donnee[hauteur-1][j] = 8;
    }*/
	return 1;
}

int getDonnee(int x, int y){
	return Donnee[x][y];
}

void setDonnee(int x, int y, int value){
	Donnee[x][y] = value;
}

void affichage(int largeur, int hauteur){
    unsigned char caractere[2];
    caractere[0] = 218;
    caractere[1] = 191;
    int i,j;
    for (i=0;i<hauteur;i++){ // affichage
        for(j=0;j<largeur;j++){
            if(Donnee[i][j]==0){ // definit le vide (0)
                printf("  ");
            }else if (Donnee[i][j] == 6){//les bords (1 et 2)
                if(j == 0)
                    printf("| ");
                if(j == largeur-1)
                    printf(" |");
            }else if (Donnee[i][j] == 5){//les bords (1 et 2)
                printf("--");
            }else if (i==0 && j==0){ //les coins (3)
                printf("%c-",caractere[0]);
            }else if (i==0 && j==largeur-1){ //les coins (3)
                printf("-%c",caractere[1]);
            }else if (Donnee[i][j]>=1 && Donnee[i][j]<=4){ // affiche les briques
                printf("%d ", Donnee[i][j]);
            }else if (Donnee[i][j]==8){
                printf("__");
            }else if (Donnee[i][j]==9){
                printf("xx");
            }
        }
        printf("\n");
    }
}

int getNumberOfbrick(int largeur, int hauteur){
	int i, j, n = 0;
	for(i = 0; i < hauteur; i++){
		for(j = 0; j < largeur; j++){
			if(Donnee[i][j] > 0 && Donnee[i][j] <= 3){
				n++;
			}
		}
	}
	return n;
}

int initialiserFichier(int level){
	FILE *fichier = NULL;
	char uri[15];
	sprintf(uri, "niveau%d.txt", level);
	fichier = fopen(uri, "r");
	if(fichier != NULL){
		int c = NULL;
		int i = 0, j = 0;
		while(c != EOF && c != -1){
            c = fgetc(fichier);
            if(c == '\n'){
				i++;
				j = 0;
			}else if(char2int(c) != NULL){
				Donnee[i][j] = char2int(c);
				j++;
			}else{
				Donnee[i][j] = 0;
				j++;
			}

        }
	}else{
		return 0;
	}
	return 1;
}

int char2int(char c){
    if(c >= 48 && c <= 57)
        return c - 48;
	if(c >= 97 && c <= 122)
		return c - 97 + 10;
	if(c >= 65 && c <= 90)
		return c - 65 + 36;
    return NULL;
}