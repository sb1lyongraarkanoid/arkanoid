#include <stdio.h>
#include <stdlib.h>
#include "Donn�e.h"
#include "Jeu.h"
#include <time.h>

int data[3][2];

int posBallX[4], posBallY[4], dirBallX[4], dirBallY[4], velBall, sizeBall;

int posPadX, posPadY, velPad, sizePad;

int isSticky, hasSticky;

int hasDestruct;

int isDeed, hasLife, level, hasLevelUp;

int bonus[10][3];
int nbBonus, bonusSize;
int isExplosive, timeVelBall, timeSizePad, hasBall;
int needPadBonus;

int score;

//. ...number(Width/Height)..brick(Width/Height)..window(Width/Height)
void initData(int nW, int nH, int bW, int bH, int wW, int wH ){
	data[0][0] = nW; //number of brick Width
	data[0][1] = nH; //----------------Height
	data[1][0] = bW; //size in pixel of a brick Width
	data[1][1] = bH; //------------------------Height
	data[2][0] = wW; //size in pixel of the windows Width
	data[2][1] = wH; //----------------------------Height

	score = 0;
	level = 0;
	hasLevelUp = 0;

	//Default state
	initialiser(data[0][0], data[0][1], level);

	initValue();
}

void initValue(){
	posBallX[0] = data[2][0] / 2;
	posBallY[0] = data[2][1] - data[1][1];
	dirBallX[0] = 0;
	dirBallY[0] = -5;
	velBall = 2;
	//multiball
	int i;
	for(i = 1; i < 4; i++){
		posBallX[i] = 0;
		posBallY[i] = 0;
		dirBallX[i] = 0;
		dirBallY[i] = 0;
	}

	sizeBall = 18;
	isSticky = 1;
	hasSticky = 0;
	hasDestruct = 0;

	sizePad = 140;
	posPadX = (data[2][0] / 2) - (sizePad / 2);
	posPadY = 5 + (data[0][1] - 1) * data[1][1] + 1;
	velPad = 8;

	isDeed = 0;
	hasLife = 3;

	int j;
	for(i = 0; i < 10; i++){
		for(j = 0; j < 3; j++){
			bonus[i][j] = 0;
		}
	}
	nbBonus = 0;
	bonusSize = 80;
	isExplosive = 0;
	timeVelBall = 0;
	timeSizePad = 0;
	hasBall = 1;
	needPadBonus = 0;
}

int getBallX(int nb){
	return posBallX[nb];
}

int getBallY(int nb){
	return posBallY[nb];
}

int getBallGo(int nb){
	if(dirBallX[nb] == 0 && dirBallY[nb] == 0){
		return 0;
	}
	return 1;
}

void dropBall(int nb){
	posBallX[nb] = 0;
	posBallY[nb] = 0;
}

int getBallVel(){
	return velBall;
}

int goBall(int nb){
	//ballExist
	if(dirBallX[nb] == 0 && dirBallY[nb] == 0){
		return 0;
	}
	//traitement bonus
	if(timeVelBall != 0 && timeVelBall - (int)time(NULL) < 0){
		velBall = 2;
		timeVelBall = 0;
	}
	if(timeSizePad != 0 && timeSizePad - (int)time(NULL) < 0){
		posPadX = posPadX  + (sizePad-140)/2;
		sizePad = 140;
		timeSizePad = 0;
		needPadBonus = 1;
	}

	//Sticky ball
	if(isSticky == 1 && hasBall == 1){
		posBallX[nb] = posPadX + (sizePad / 2) - (sizeBall / 2);
		posBallY[nb] = posPadY - sizeBall;
		dirBallX[nb] = 0;
		dirBallY[nb] = -5;
		return 0;
	}else if(isSticky == 2){
		posBallY[nb] = posPadY - sizeBall;
		return 0;
	}
	posBallX[nb] += dirBallX[nb] * velBall;
	posBallY[nb] += dirBallY[nb] * velBall;
	
	//rebond pad
	if(posBallX[nb] <= 0){
		dirBallX[nb] = -dirBallX[nb] ;
		posBallX[nb] = 0;
	}
	
	if(posBallX[nb] >= data[2][0] - sizeBall){
		dirBallX[nb] = -dirBallX[nb] ;
		posBallX[nb] = data[2][0] - sizeBall;
	}
	
	if(posBallY[nb] <= 0){
		dirBallY[nb] = -dirBallY[nb];
		posBallY[nb] = 0;
	}

	//Tape sur le pad
	if((posBallY[nb] + sizeBall) >= posPadY && (posBallX[nb] + (sizeBall / 2)) >= posPadX && (posBallX[nb] + (sizeBall / 2)) <= (posPadX + sizePad)){
		if(hasSticky > 0){
			hasSticky--;
			isSticky = 2;
		}
		//Nombre demi-zone 4 = 4 zones n�gative + 4 zones positive + 1 vertical
		int dzones = 4;
		int zones = dzones * 2 + 1;
		//part en pixel
		int pp = sizePad / zones;
		//restant part pixel gauche (modulo / 2 + reste)
		int pr = (sizePad % zones)/2 + (sizePad % zones)%2;
		//restant part pixel droite (modulo / 2)
		int prend = (sizePad % zones)/2;

		int centerBallX = posBallX[nb] + (sizeBall/2 + sizeBall%2);

		//xs = x_start / xe = x_end
		int i, xs, xe, dx = -dzones, dy = -1;
		for(i = 0; i < zones; i++){
			//DEBUT
			if(i == 0){
				xs = 0;
			}else{
				xs = pp*i+pr;
			}
			if(i == zones - 1){
				xe = pp*(i+1)+pr+prend;
			}else{
				xe = pp*(i+1)+pr;
			}
			//EXEC
			if(centerBallX >= posPadX + xs && centerBallX <= posPadX + xe){
				dirBallX[nb] = dx;
				dirBallY[nb] = dy;
				break;
			}
			//FIN
			dx++;
			if(dx > 0){
				dy++;
			}else{
				dy--;
			}
		}

	}

	//mort
	if(hasBall > 1 && posBallY[nb] >= data[2][1] - sizeBall){
		hasBall--;
		int tmpX = posBallX[nb], tmpY = posBallY[nb];
		int i;
		for(i = nb; i < hasBall; i++){
			posBallX[i] = posBallX[i+1];
			posBallY[i] = posBallY[i+1];
			dirBallX[i] = dirBallX[i+1];
			dirBallY[i] = dirBallY[i+1];
		}
		posBallX[hasBall] = tmpX;
		posBallY[hasBall] = tmpY;
		dirBallX[hasBall] = 0;
		dirBallY[hasBall] = 0;
	}else{
		if(posBallY[nb] >= data[2][1] - sizeBall){
			hasLife--;
			hasSticky = 0;
			isExplosive = 0;
			if(hasLife > 0){
				posBallX[nb] = posPadX + (sizePad / 2) - (sizeBall / 2);
				posBallY[nb] = posPadY - sizeBall;
				dirBallX[nb] = 0;
				dirBallY[nb] = -5;
				isSticky = 1;
			}else{
				isDeed = 1;
				dirBallX[nb] = 0;
				dirBallY[nb] = 0;
			}
		}
	}

	//colision brick
	testColisionsBall(nb);
	if(hasDestruct == 1){
		hasDestruct = 0;
		return 1;
	}
	return 0;
}

void startBall(){
	if(isSticky >= 1)
		isSticky = 0;
}

int getPadX(){
	return posPadX;
}

int getPadSize(){
	return sizePad;
}

int getPadVel(){
	return velPad;
}

void goPad(int dir){
	if(dir == 1){
		posPadX += velPad;
		if(posPadX >= data[2][0] - 5 - data[1][0] - sizePad)
			posPadX = data[2][0] - 5 - data[1][0] - sizePad;
		if(isSticky == 2 && !(posPadX >= data[2][0] - 5 - data[1][0] - sizePad) && hasBall == 1){
			posBallX[0] += velPad;
			if(posBallX[0] + sizeBall >= data[2][0] - 5 - data[1][0]){
				posBallX[0] = data[2][0] - 5 - data[1][0] - sizeBall;
			}
		}
	}else{
		posPadX -= velPad;
		if(posPadX <= 5 + data[1][0])
			posPadX = 5 + data[1][0];
		if(isSticky == 2 && !(posPadX <= 5 + data[1][0]) && hasBall == 1){
			posBallX[0] -= velPad;
			if(posBallX[0] <= 5 + data[1][0]){
				posBallX[0] =  5 + data[1][0];
			}
		}
	}
}

void goPadJoy(int vel){
	posPadX += vel;
	if(posPadX >= data[2][0] - 5 - data[1][0] - sizePad)
		posPadX = data[2][0] - 5 - data[1][0] - sizePad;
	if(posPadX <= 5 + data[1][0])
		posPadX = 5 + data[1][0];
	if(isSticky == 2 && !(posPadX <= 5 + data[1][0]) && !(posPadX >= data[2][0] - 5 - data[1][0] - sizePad) && hasBall == 1){
		posBallX[0] += vel;
		if(posBallX[0] <= 5 + data[1][0]){
			posBallX[0] =  5 + data[1][0];
		}
		if(posBallX[0] + sizeBall >= data[2][0] - 5 - data[1][0]){
			posBallX[0] = data[2][0] - 5 - data[1][0] - sizeBall;
		}
	}
}


//Capteur Colisions
int pointIsPositive(int k, int l, int nb){
	int x = posBallX[nb] + k * sizeBall;
	int y = posBallY[nb] + l * sizeBall;

	int i = (x - 5) / data[1][0];
	int j = (y - 5) / data[1][1];

	if(i >=0 && i < data[0][0] && j >= 0 && j < data[0][1]){
		if(getDonnee(j, i) > 0 && getDonnee(j, i) < 8){
			int state = getDonnee(j, i);
			//dustruction brick
			if(getDonnee(j, i) <= 3 && getDonnee(j, i) >= 0){
				hasDestruct = 1;
			}
			return state * 10000 + j * 100 + i;
		}else{
			return 0;
		}
	}
}

int sideIsPositive(int k, int l, int nb){
	int x = posBallX[nb] + k * sizeBall;
	int y = posBallY[nb] + l * sizeBall;

	int i = (x - 5) % data[1][0];
	int j = (y - 5) % data[1][1];

	if(k == 0){
		i = data[1][0] - i;
	}
	if(l == 0){
		j = data[1][1] - j;
	}

	if(i > j){
		dirBallY[nb] = -dirBallY[nb];
		return -1;
	}else if(i < j){
		dirBallX[nb] = -dirBallX[nb];
		return 1;
	}else{
		dirBallX[nb] = -dirBallX[nb];
		dirBallY[nb] = -dirBallY[nb];
		return 0;
	}
}

int getIsDeed(){
	if(isDeed){
		//Default state
		return 1;
	}
	return 0;
}

int getLife(){
	return hasLife;
}

void levelUp(){
	hasLevelUp = 1;
	level++;
	initialiser(data[0][0], data[0][1], level);
	initValue();
}

int getLevel(){
	return level;
}

void resetLevel(){
	level = 0;
}

int getHasLevelUp(){
	if(hasLevelUp){
		hasLevelUp = 0;
		return 1;
	}
	return 0;
}

void testColisionsBall(int nb){
	/*
	 * A  B
	 * +--+
	 * |  |
	 * +--+
	 * D  C
	 */
	int A, B, C, D;
	A = pointIsPositive(0, 0, nb);
	B = pointIsPositive(1, 0, nb);
	C = pointIsPositive(1, 1 ,nb);
	D = pointIsPositive(0, 1, nb);

	int x, y;
	x = dirBallX[nb];
	y = dirBallY[nb];

	int tmp;

	if((A && B && C) || (B && C && D) || (C && D && A) || (D && A && B)){
		dirBallX[nb] = -dirBallX[nb];
		dirBallY[nb] = -dirBallY[nb];
	}else if((A && B) || (D && C)){
		dirBallY[nb] = -dirBallY[nb];
	}else if((A && D) || (B && C)){
		dirBallX[nb] = -dirBallX[nb];
	}else if(A){ //Haut Gauche
		if(x >= -4 && x <= -1 && y >= 0 && y <= 4){
			dirBallX[nb] = -dirBallX[nb];
		}else if(x >= -4 && x <= -1 && y >= -4 && y <= -1){
			tmp = sideIsPositive(0, 0, nb);
		}else if(x >= 0 && x <= 4 && y >= -5 && y <= -1){
			dirBallY[nb] = -dirBallY[nb];
		}
	}else if(B){ //Haut Droite
		if(x >= -4 && x <= 0 && y >= -5 && y <= -1){
			dirBallY[nb] = -dirBallY[nb];
		}else if(x >= 1 && x <= 4 && y >= -4 && y <= -1){
			tmp = sideIsPositive(1, 0, nb);
		}else if(x >= 1 && x <= 4 && y >= 1 && y <= 4){
			dirBallX[nb] = -dirBallX[nb];
		}
	}else if(C){
		if(x >= 1 && x <= 4 && y >= -4 && y <= -1){
			dirBallX[nb] = -dirBallX[nb];
		}else if(x >= 1 && x <= 4 && y >= 1 && y <= 4){
			tmp = sideIsPositive(1, 1, nb);
		}else if(x >= -4 && x <= 0 && y >= 1 && y <= 5){
			dirBallY[nb] = -dirBallY[nb];
		}
	}else if(D){
		if(x >= 0 && x <= 4 && y >= 1 && y <= 5){
			dirBallY[nb] = -dirBallY[nb];
		}else if(x >= -4 && x <= -1 && y >= 1 && y <= 4){
			tmp = sideIsPositive(0, 1, nb);
		}else if(x >= -4 && x <= -1 && y >= -4 && y <= -1){
			dirBallX[nb] = -dirBallX[nb];
		}
	}

	//Destruction
	if(hasDestruct){
		if(A && A/10000 < 4 && A/10000 >=0){
			score += (isExplosive)?25:1;
			setDonnee((A%10000) / 100,  A % 100, getDonnee((A%10000) / 100,  A % 100) - 1);
			if(getDonnee((A%10000) / 100,  A % 100) == 0){addRandomBonus(A % 100, (A%10000) / 100);}
		}
		if(B != A && B && B/10000 < 4 && B/10000 >=0){
			score += (isExplosive)?25:1;
			setDonnee((B%10000) / 100,  B % 100, getDonnee((B%10000) / 100,  B % 100) - 1);
			if(getDonnee((B%10000) / 100,  B % 100) == 0){addRandomBonus(B % 100, (B%10000) / 100);}
		}
		if((C != B && C != A) && C && C/10000 < 4 && C/10000 >=0){
			score += (isExplosive)?25:1;
			setDonnee((C%10000) / 100,  C % 100, getDonnee((C%10000) / 100,  C % 100) - 1);
			if(getDonnee((C%10000) / 100,  C % 100) == 0){addRandomBonus(C % 100, (C%10000) / 100);}
		}
		if((D != C && D != B && D != A) && D && D/10000 < 4 && D/10000 >=0){
			score += (isExplosive)?25:1;
			setDonnee((D%10000) / 100,  D % 100, getDonnee((D%10000) / 100,  D % 100) - 1);
			if(getDonnee((D%10000) / 100,  D % 100) == 0){addRandomBonus(D % 100, (D%10000) / 100);}
		}
	}

	//bonus explosive
	if(hasDestruct && isExplosive){
		if(A && A/10000 < 4 && A/10000 >=0){
			int y = (A%10000) / 100; int x = A % 100, i, j;
			for(i = -1; i <= 1; i++){
				for(j = -1; j <= 1; j++){
					if(getDonnee(y+j ,x+i) < 4){
						setDonnee(y+j ,x+i, 0);
					}
				}
			}
		}else if(B != A && B && B/10000 < 4 && B/10000 >=0){
			int y = (B%10000) / 100;
			int x = B % 100, i, j;
			for(i = -1; i <= 1; i++){
				for(j = -1; j <= 1; j++){
					if(getDonnee(y+j ,x+i) < 4){
						setDonnee(y+j ,x+i, 0);
					}
				}
			}
		}else if((C != B && C != A) && C && C/10000 < 4 && C/10000 >=0){
			int y = (C%10000) / 100; int x = C % 100, i, j;
			for(i = -1; i <= 1; i++){
				for(j = -1; j <= 1; j++){
					if(getDonnee(y+j ,x+i) < 4){
						setDonnee(y+j ,x+i, 0);
					}
				}
			}
		}else if((D != C && D != B && D != A) && D && D/10000 < 4 && D/10000 >=0){
			int y = (D%10000) / 100; int x = D % 100, i, j;
			for(i = -1; i <= 1; i++){
				for(j = -1; j <= 1; j++){
					if(getDonnee(y+j ,x+i) < 4){
						setDonnee(y+j ,x+i, 0);
					}
				}
			}
		}
	}

	if(x != dirBallX[nb]){
		if(x > 0){
			posBallX[nb] - (velBall * x);
		}else{
			posBallX[nb] + (velBall * x);
		}
	}
	if(y != dirBallY[nb]){
		if(y > 0){
			posBallY[nb] - (velBall * y);
		}else{
			posBallY[nb] + (velBall * y);
		}
	}
}

void addRandomBonus(int x, int y){
	if(nbBonus < 10){
		int random = rand()%10;
		int probability = rand()%100;
		if(level <= 2 && probability >= 95){
			random = 0;
		} else if(level <= 5 && probability >= 85){
			random = 0;
		} else if(level <= 7 && probability >= 75){
			random = 0;
		} else if(probability >= 50){
			random = 0;
		}

		if(random > 0){
			bonus[nbBonus][0] = random;
			bonus[nbBonus][1] = x * data[1][0] + 5;
			bonus[nbBonus][2] = y * data[1][1] + 5;
			nbBonus++;
		}
	}
}

int getBonusNb(){
	return nbBonus;
}

int getBonusN(int n){
	return bonus[n][0];
}

int getBonusX(int n){
	return bonus[n][1];
}

int getBonusY(int n){
	return bonus[n][2];
}

void goBonus(){
	int i;
	for(i = 0; i < nbBonus; i++){
		bonus[i][2]++;
		//Si le bonus touche le pad
		if(bonus[i][2] + data[1][1] >= posPadY && bonus[i][1] + ((data[1][0] - bonusSize) /2) + bonusSize >= posPadX && bonus[i][1] + (data[1][0] - bonusSize) /2 <= posPadX + sizePad){
			switch (bonus[i][0])
			{
			case 1:
				posPadX = posPadX  + (sizePad-210)/2;
				timeSizePad = (timeSizePad - (int)time(NULL) > 0 && sizePad == 210)?timeSizePad + 20:(int)time(NULL) + 20;
				sizePad = 210;
				if(timeSizePad - (int)time(NULL) > 60){
					timeSizePad = (int)time(NULL) + 60;
				}
				needPadBonus = 1;
				break;
			case 2:
				posPadX = posPadX  + (sizePad-70)/2;
				timeSizePad = (timeSizePad - (int)time(NULL) > 0 && sizePad == 70)?timeSizePad + 20:(int)time(NULL) + 20;
				sizePad = 70;
				if(timeSizePad - (int)time(NULL) > 60){
					timeSizePad = (int)time(NULL) + 60;
				}
				needPadBonus = 1;
				break;
			case 3:
				hasBall++;
				hasSticky = 0;
				isSticky = 0;
				if(hasBall > 4){
					hasBall = 4;
				}else{
					int direction = rand()%9;
					posBallX[hasBall-1] = posBallX[0] - dirBallX[0] * velBall;
					posBallY[hasBall-1] = posBallY[0] - dirBallY[0] * velBall;
					dirBallX[hasBall-1] = -dirBallX[0];
					dirBallY[hasBall-1] = dirBallY[0];
				}
				break;
			case 4:
				timeVelBall = (timeVelBall > (int)time(NULL) && velBall > 2)?timeVelBall + 20:(int)time(NULL) + 20;
				velBall++;
				if(velBall > 6){
					velBall = 6;
				}
				if(timeVelBall - (int)time(NULL) > 60){
					timeVelBall = (int)time(NULL) + 60;
				}
				break;
			case 5:
				timeVelBall = (timeVelBall > (int)time(NULL) && velBall < 2)?timeVelBall + 20:(int)time(NULL) + 20;
				velBall = 1;
				if(timeVelBall - (int)time(NULL) > 60){
					timeVelBall = (int)time(NULL) + 60;
				}
				break;
			case 6:
				if(hasBall != 1){
					break;
				}
				hasSticky += 5;
				if(hasSticky > 15)
					hasSticky = 15;
				break;
			case 7:
				isExplosive = 1;
				break;
			case 8:
				hasLife--;
				if(hasLife == 0){
					isDeed = 1;
				}
				break;
			case 9:
				hasLife++;
				if(hasLife > 3){
					hasLife = 3;
				}
				break;
			default:
				break;
			}
			bonus[i][0] = -1;
		}
		//Si le bonus tombe en bas
		if(bonus[i][2] >= data[2][1] - data[1][1])
		{
			bonus[i][0] = -1;
		}
	}
}

int getDeleteBonusNb(){
	int i, n = 0;
	for(i = 0; i < nbBonus; i++){
		if(bonus[i][0] == -1){
			n++;
		}
	}
	return n;
}

int getDeleteBonusX(){
	int i = 0, r = 0;
	r = bonus[i][1];
	while(bonus[i][0] != -1){
		i++;
		r = bonus[i][2];
	}
	return r;
}

int getDeleteBonusY(){
	int i = 0, r = 0;
	r = bonus[i][2];
	while(bonus[i][0] != -1){
		i++;
		r = bonus[i][2];
	}
	return r;
}

void goDeleteBonus(){
	int i = 0;
	while(bonus[i][0] != -1){
		i++;
	}
	int j;
	for(j = i; j < nbBonus; j ++){
		bonus[j][0] = bonus[j+1][0];
		bonus[j][1] = bonus[j+1][1];
		bonus[j][2] = bonus[j+1][2];
	}
	nbBonus--;
}

int getPadBonusFlag(){
	if(needPadBonus){
		needPadBonus = 0;
		return 1;
	}
	return 0;
}

int getIsExplosive(){
	return isExplosive;
}

int getTimeVelBall(){
	return (timeVelBall - (int)time(NULL) <= 0)?0:timeVelBall - (int)time(NULL);
}

int getTimeSizePad(){
	return (timeSizePad - (int)time(NULL) <= 0)?0:timeSizePad - (int)time(NULL);
}

int getHasSticky(){
	return hasSticky;
}

int getHasDoubleBall(){
	return hasBall;
}

int getScore(){
	return score;
}