#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

//int screenWidth, screenHeight, brickWidth, brickHeight;


/*
 * Initialise la fen�tre de jeu
 * (nombreBriqWidth, nombreBriqHeight, briqWeight, briqHeight)
 */

void initWindows(int sW, int sH, int bW, int bH);

void addBrick(int x, int y);

void displayAllBrick();

void setBackground(int);

void displayBall();

void displayPad();

void oldDataExchange();

void firstDataExchange();

void brickExchange();

void eraseBrick();

void diplayBonus();

void displayStat();

void clearScreen();

void merge2screen(SDL_Surface* surface, SDL_Rect position);

void merge2save(SDL_Surface* surface, SDL_Rect position);

void merge2saveBonus(SDL_Surface* surface, SDL_Rect position);

void send2Screen();

void arkanoidFullDisplay();

void closeWindows();

void event();

void waitScreen(char file[50], int hideStat);

void demanderID();

#endif // DONN�ES_H_INCLUDED
